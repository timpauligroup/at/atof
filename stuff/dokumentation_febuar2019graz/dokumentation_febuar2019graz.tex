\documentclass[a4paper, 12pt]{scrartcl}
% packages
% std encoding
\usepackage[utf8]{inputenc}
\usepackage[german]{babel}
\usepackage[T1]{fontenc}
% nice text
\usepackage{lmodern}
\renewcommand\familydefault{\sfdefault}
\usepackage{microtype}
\usepackage{float}
\setlength{\parskip}{0em}
\setlength{\parindent}{0em}
%
\usepackage[pdftitle={Live-Elektronik Pauli},pdfauthor={Tim Pauli}, linktocpage, unicode]{hyperref}
\usepackage[]{geometry}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage[german]{fancyref}

\title{Dokumentation zur Klanginstallation \textit{atof} (Stand März 2019)}
\author{Tim Pauli}
\date{}

\begin{document}
\maketitle

\textbf{Einleitung}

\textit{atof} ist (in dieser Iteration) eine Klanginstallation für 2 Mikrofone, 4 Lautsprecher und einen Stuhl mit Kontaktmikrofon unter der Sitzfläche (und Computer), bei welcher ein Raum mit sich selbst Feedback erzeugt
und unterschiedliche klangliche Transformationen durchläuft.
Damit der Pure-Data-Patch ohne Probleme lädt, muss zuerst die Datei \textit{atof/fx/mono/atof~/atof~.c} kompiliert werden.
Dazu muss im Ordner der Datei \textit{make} ausgeführt werden.
Der Patch kann gestartet werden mit der Datei \textit{main.pd}.
\\

\textbf{Klangsynthese}

\begin{figure}[ht]
    \includegraphics[trim={1cm 16cm 1cm 0cm}, clip, width=\textwidth]{flowchart_atof}
    \caption{Signalflussdiagram von \textit{atof}}
    \label{fig:flowchartatof}
\end{figure}

In \fref{fig:flowchartatof} ist vereinfacht der Signalfluss der Klangsynthese dargestellt.
\textit{feedback control input} versucht das eingehende Mikrofonsignal leiser zu machen.
Je lauter der Eingangspegel wird, umso schneller versucht dieses Modul gegenzupegeln.
Dazu kommen Notch-Filter, welche versuchen die prägnantesten Frequenzen herauszufiltern.
\textit{feedback control output} versucht als letztes Modul in der Signalkette zu erreichen, dass der Ausgangspegel nicht zu leise wird.
Je leiser der Ausgangspegel wird, umso schneller versucht dieses Modul das Signal zu verstärken.

\begin{figure}[ht]
    \includegraphics[trim={1cm 16cm 1cm 0cm}, clip, width=\textwidth]{flowchart_fx}
    \caption{Signalflussdiagram von \textit{fx} aus \ref{fig:flowchartatof}}
    \label{fig:flowchartfx}
\end{figure}

Das Innenleben von \textit{fx} wird in \fref{fig:flowchartfx} anschaulich dargelegt.
Zwischen zwei Einheiten von Comb-Filter (Pre- und Posteffekte) mit anschließender Amplitudenmodulation sitzt das Herzstück der Installation:
das \textit{atof\textasciitilde} Objekt.
\textit{atof\textasciitilde} interpretiert jeden eingehenden 32-Bit-Float (diskrete Punkte der Membranschwingung als digitales Audio) als String mit 4 Chars,
um ihn anschließend mit Hilfe der C-Funktion \textit{atof}, wieder in einen 32-Bit-Float zu verwandeln.
Die C-Funktion \textit{atof} ist eigentlich dazu da die Kombinationen von Chars, welche den Zahlen entsprechen, in ihre äquivalenten numerischen Float zu überführen.
Zum Beispiel  	\grqq4\grqq zum Float $4$ und \grqq0.001\grqq zum Float $0.001$.
Falls das nicht möglich ist, gibt die Funktion einfach $0$ zurück.

Da es sehr viel mehr vierstellige Strings gibt, die keinen Zahlen entsprechen, gibt das \textit{atof\textasciitilde} Objekt sehr oft $0$ aus.
Falls aber ein Eingabewert dennoch auf eine Zahl abbildet, wird konstant der gleiche Wert zurückgegeben.
Im Grunde entspricht diese Form der Klangtransformationen einer Art Waveshaping mit einer Tabelle von $2^{32}$ Werten, in der sehr viele Werte $0$ sind.
Des Weiteren lässt sich noch über eine Tabelle einstellen, auf welchen Wertebereiche der eingehenden Schallwelle die Transformation angewendet werden soll und wie stark sie skaliert werden sollen (maximal Faktor 1).
Zum Beispiel lässt sich so einstellen, dass wie bei einer klassischen Distortion nur die stärksten Ausschläge transformiert werden
(bei einer auf $-1$ bis $1$ normalisierten Welle beispielsweise $0.9$ bis $1$).
Es sind aber auch speziellere Szenarien denkbar wie zum Beispiel nur auf die schwächsten Ausschläge und die welche ziemlich genau in der Mitte liegen
(bei einer auf $-1$ bis $1$ normalisierten Welle beispielsweise $-0.1$ bis $0.1$, $-0.51$ bis $-0.49$ und  $0.49$ bis $0.51$).
Welche Wertebereiche clean durchkommen und welche transformiert werden, lässt sich getrennt einstellen (im Folgenden Clean- und Dirty-Tablle genannt).
Wenn keines von beiden zutrifft, wird auch einfach $0$ zurückgegeben.
\\

\textbf{Zeitliche Struktur}

Die Installation bewegt sich grundlegend die gesamte Zeit durch einen Zyklus von $8$ Phasen.
Unter der Prämisse, dass eine Person beim Setzen und Aufstehen vom Stuhl Geräusche erzeugt, werden Phase $1$, $3$, $6$, $7$ durch ausreichend gemessene Aktivität am Kontaktmikrofon gestartet.
Aus dieser Messung werden auch die darauffolgenden Dauern der Phasen berechnet.
Anfänglich sind nur die Preeffekte aktiv:

\begin{enumerate}
    \item langsamer Übergang zum \textit{atof\textasciitilde} Klang
    \item Neuberechnung der Parameter der Pre- und Posteffekte
    \item langsamer Weggang vom \textit{atof\textasciitilde} Klang
    \item Wechsel von Pre- zu Posteffekten (mit gleichen Parametern)
    \item Neuberechnung der Parameter von \textit{atof\textasciitilde}
    \item langsamer Übergang zum \textit{atof\textasciitilde} Klang
    \item langsamer Weggang vom \textit{atof\textasciitilde} Klang
    \item Wechsel von Post- zu Preeffekten
\end{enumerate}

Die Neuberechnung der Parameter der Pre- und Posteffekte erfolgt auf Basis der stärksten gemessenen Frequenzen und des Amplituden-RMS in der davorliegenden Zeit.
Die Neuberechnung der Parameter für \textit{atof\textasciitilde} basiert auf dem Maximum eines einzigen Audio-Buffers (eine Art von Pseudozufall).
Diese Neuberechnung verwirft allerdings nicht die vorherigen Parameter, sondern fügt die neuen Wertebereiche, die transformiert werden sollen, der Dirty-Tabelle immer weiter hinzu.
Dadurch wird der Effekt von \textit{atof\textasciitilde} über den Verlauf der Installation immer stärker.
Der Übergangsalgorithmus von \textit{atof\textasciitilde} funktioniert folgendermaßen, nachdem eine neue Zieltabelle für die Dirty-Tabelle feststeht:

\begin{enumerate}
    \item Morph der Clean-Tabelle, welche zu diesem Zeitpunkt nur Einsen beinhaltet, zur Inversen der Dirty-Tabelle
    \item Morph der Dirty-Tabelle, welche zu diesem Zeitpunkt nur Nullen beinhaltet, zur Zieltabelle
    \item Morph der Clean-Tabelle zu einer Tabelle voller Nullen
\end{enumerate}

Beim Weggang werden alle Schritte umgedreht.
Der benutzte Morph fängt je nach Über- oder Weggang am oberen oder unteren Wertebereich der Tabelle an und fügt nacheinander die Zielwerte ein.
Je näher der aktuell bearbeitete Index allerdings dem aktuell gemessenen Amplituden-RMS liegt, umso langsamer werden die neuen Werte eingefügt.

\end{document}