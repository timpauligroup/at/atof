from google.cloud import texttospeech

client = texttospeech.TextToSpeechClient()


def speak(client, text, name, pitch, rate):
    # ssml = '<speak>  Step 1, take a deep breath. <break time="1000ms"/> Step 2, exhale. Step 3, take a deep breath again. <break strength="weak"/> Step 4, exhale. </speak>'
    synth_input = texttospeech.types.SynthesisInput(text=text)
    voice = texttospeech.types.VoiceSelectionParams(
        language_code='de-DE',
        name=name)
    audio_config = texttospeech.types.AudioConfig(
        audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16,
        speaking_rate=rate,
        pitch=pitch,
        sample_rate_hertz=48000)
    response = client.synthesize_speech(
        synth_input, voice, audio_config)
    output_name = name + '/' + text.replace(" ", "_") + '_' + \
        str(rate) + '_' + str(pitch) + '.wav'
    with open(output_name, 'wb') as out:
        out.write(response.audio_content)
    return output_name


def speak_alot(client, text, files, folder):
    rate = 0.25
    pitch = 19
    output_names = []
    for name in ['de-DE-Wavenet-A', 'de-DE-Wavenet-C', 'de-DE-Wavenet-B', 'de-DE-Wavenet-D']:
        output_names.append(speak(client, text, name, pitch, rate))
    for n, f in zip(output_names, files):
        f.write(folder + n + ';\n')


with open("de-DE-Wavenet-A/samples.txt", "w+") as a, open("de-DE-Wavenet-B/samples.txt", "w+") as b, open("de-DE-Wavenet-C/samples.txt", "w+") as c, open("de-DE-Wavenet-D/samples.txt", "w+") as d:
    files = [a, b, c, d]
    speak_alot(client, "sprich mit mir", files, '../voice/')

    speak_alot(client, "rede mit mir", files, '../voice/')

    speak_alot(client, "komm naeher", files, '../voice/')

    speak_alot(client, "komm zu mir", files, '../voice/')

    speak_alot(client, "ich will dich spueren", files, '../voice/')

    speak_alot(client, "ich will dich fuehlen", files, '../voice/')

    speak_alot(client, "beruehre mich", files, '../voice/')

    speak_alot(client, "fass mich an", files, '../voice/')

    speak_alot(client, "ich kann dich nicht hoeren", files, '../voice/')

    speak_alot(client, "wo bist du?", files, '../voice/')
